import pytest
import httpx
from gsheet_service.views import app


@pytest.fixture
def client():
    return httpx.Client(app=app, base_url="http://test-server")


@pytest.mark.asyncio
async def test_get_cloudinary_image_when_not_exist(client: httpx.Client):
    response = await client.get(
        "/media/images",
        params={
            "url": "https://www.dropbox.com/s/l0yygsrxbxffxsl/Q1.jpg?raw=1",
            "provider": "cloudinary",
        },
    )
    assert response.status_code == 200 
    assert response.json == {
        'status':True,
        'data':{
            "service": None,
            "resource_id": "mj82ekkke0gktkx9dhr0",
            "kind": "image"
            }
    }


def gpa():
        # all the parameters needed for the calculator
    total_score = 0
    grade_gotten = 0
    number_of_courses = int(input("Enter total number of courses offered: "))
    print("Good job")
    for x in range(number_of_courses):
        course = input("Enter course code: ")
        unit = int(input("Enter course unit: "))
        score = int(input("Enter your score: "))
        print ("calculating")
        total_score += unit
    # grade range(70-100 = 5 points, 60-69 = 4 points, 50-59 = 3 points, 45-49 = 2 points, 40-44 = 1 points, <39 = 0 points)
        if (score >= 70):
            grade = 5
        elif (score < 70 and score >= 60):
            grade = 4
        elif (score < 60 and score >= 50):
            grade = 3
        elif (score < 50 and score >= 45):
            grade = 2
        elif (score < 45 and score >= 40):
            grade = 1
        else :
            grade = 0
        grade_gotten += unit * grade
    gpa = float(grade_gotten/total_score)
    print ("Your GPA is: " + str(gpa))