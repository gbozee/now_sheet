from gsheet_service.views import app as asgi_app

import sentry_sdk
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware


sentry_sdk.init(dsn="https://c682bb5318694a06992190f8d1ecf41f@o1104616.ingest.sentry.io/6131892")

app = SentryAsgiMiddleware(asgi_app)
